from django.contrib import admin
from .models import Bin, Operation

# Register your models here.

class BinAdmin(admin.ModelAdmin):

    list_display = ['latitude', 'longitude', 'last_collection', 'operation_id']
    list_display_links = ['latitude', 'longitude']

class OptAdmin(admin.ModelAdmin):

    list_display = ['id', 'name']
    list_display_links = ['name']

    class Meta:
        model = Bin

admin.site.register(Bin, BinAdmin)
admin.site.register(Operation, OptAdmin)