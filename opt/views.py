from django.shortcuts import render, HttpResponse, redirect, get_object_or_404
from .models import Bin
from datetime import datetime, timedelta

# Create your views here.
def index(request):
    bins = Bin.objects.all()

    return render(request, "index.html", {"bins": bins})