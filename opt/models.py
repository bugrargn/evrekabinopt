from django.db import models

# Create your models here.
class Bin(models.Model):
    operation = models.ForeignKey('Operation', on_delete=models.CASCADE, related_name='operations')
    last_collection = models.DateTimeField(verbose_name='Son Toplama', auto_now_add=True)
    latitude = models.FloatField(max_length=120, verbose_name='Enlem')
    longitude = models.FloatField(max_length=120, verbose_name='Boylam')
    collection_frequency = models.IntegerField()

    def __str__(self):
        # return '{}'.format(self.vehicle)
        return (f'{self.operation}')

    class Meta:
        ordering = ['id']

class Operation(models.Model):
    name = models.CharField(max_length=200, verbose_name='İsim')

    def __str__(self):
        return self.name
        
    class Meta:
        ordering = ['id']